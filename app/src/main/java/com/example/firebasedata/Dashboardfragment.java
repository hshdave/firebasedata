package com.example.firebasedata;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;


public class Dashboardfragment extends Fragment {


    TextView textName;
    Button btnSignout,btnDel;
    FirebaseFirestore db;
    FirebaseUser firebaseUser;

    public Dashboardfragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseUser = getArguments().getParcelable("User");
        db = FirebaseFirestore.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboardfragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textName = view.findViewById(R.id.txt_uName);
        btnSignout = view.findViewById(R.id.btn_signOut);
        btnDel = view.findViewById(R.id.btn_del);


        readDatabase();

        btnSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();

                NavController navController = Navigation.findNavController(getActivity(),R.id.nav_host_fragment);
                navController.navigate(R.id.loginfragment);
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delUser(getView());
            }
        });

    }


    public void readDatabase()
    {
        DocumentReference docref = db.collection("Users").document(firebaseUser.getUid());

        docref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()){
                    DocumentSnapshot doc = task.getResult();

                    if (doc.exists())
                    {
                        System.out.println("Document Snapshot :"+doc.getData());

                        textName.setText("Welcome "+doc.get("Name"));
                    }

                }


            }
        });


    }

    public void delUser(View dview)
    {
        View popupview = getActivity().getLayoutInflater().inflate(R.layout.popupview,null);

        final PopupWindow popupWindow = new PopupWindow(popupview,ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT,true);

        if (Build.VERSION.SDK_INT>=21)
        {
            popupWindow.setElevation(5.0f);
        }

        popupWindow.setFocusable(true);
        // popupWindow.setBackgroundDrawable(new ColorDrawable(Color.GRAY));
        popupWindow.showAtLocation(dview, Gravity.CENTER,0,0);

        final EditText edtPEmail = popupview.findViewById(R.id.edt_popemail);
        final EditText edtPPass = popupview.findViewById(R.id.edt_poppassword);
        Button btnSubmit = popupview.findViewById(R.id.btn_poplogin);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(edtPEmail.getText()))
                {
                    edtPEmail.setError("Email Cannot be blank!");
                    edtPEmail.requestFocus();
                }else if (TextUtils.isEmpty(edtPPass.getText()))
                {
                    edtPPass.setError("Password Cannot be blank!");
                    edtPPass.requestFocus();
                }else {

                    if(edtPPass.getText().toString().length()<6)
                    {
                        edtPPass.setError("Invalid Password, Password Should be at least 6 characters");
                        edtPPass.requestFocus();
                    }else {

                        AuthCredential credential = EmailAuthProvider.getCredential(edtPEmail.getText().toString(),edtPPass.getText().toString());

                        firebaseUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                if (task.isSuccessful())
                                {
                                    db.collection("Users").document(firebaseUser.getUid()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful())
                                            {
                                                firebaseUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {

                                                        if (task.isSuccessful())
                                                        {
                                                            NavController navController = Navigation.findNavController(getActivity(),R.id.nav_host_fragment);
                                                            navController.navigate(R.id.loginfragment);
                                                            popupWindow.dismiss();

                                                        }else {
                                                            Log.d("Dashboard Fragment", "onComplete: UserDelete"+task.getException().getMessage());
                                                        }

                                                    }
                                                });
                                            }


                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.d("Dashboard Fragment", "onFailure: Firestore Delete"+e.getMessage());
                                        }
                                    });
                                }
                                else {
                                    Log.d("Dashboard Fragment", "on Fail Authenticate "+task.getException().getMessage());
                                }


                            }
                        });
                    }

                }

            }
        });




    }

}
