package com.example.firebasedata;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registerfragment extends Fragment {

    EditText edt_email, edt_pass, edt_cPass, edt_name;
    Button btn_reg;
    private FirebaseAuth auth;


    public Registerfragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registerfragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edt_email = view.findViewById(R.id.edt_registerEmail);
        edt_pass = view.findViewById(R.id.edt_registerPassword);
        edt_cPass = view.findViewById(R.id.edt_registerCPassword);
        edt_name = view.findViewById(R.id.edt_registerName);

        btn_reg = view.findViewById(R.id.btn_register);

        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!checkEmptyField())
                {
                    if (edt_pass.getText().length()<6)
                    {
                        edt_pass.setError("Invalid Password, Password should be at least 6 characters!");
                        edt_pass.requestFocus();
                    }else {
                        if (!edt_pass.getText().toString().equals(edt_cPass.getText().toString()))
                        {
                            edt_cPass.setError("Password not match!");
                            edt_cPass.requestFocus();
                        }else
                        {
                            registerUser();
                        }
                    }
                }

            }
        });

    }

    public void registerUser() {
        final String email, pass, name;
        email = edt_email.getText().toString();
        pass = edt_pass.getText().toString();
        name = edt_name.getText().toString();
        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    FirebaseUser user = auth.getCurrentUser();
                    FirebaseFirestore db = FirebaseFirestore.getInstance();

                    Map<String,Object> usermap = new HashMap<>();
                    usermap.put("Name",name);
                    usermap.put("Email",email);

                /*    db.collection("Users").document(user.getUid()).set(usermap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful())
                            {
                                Toast.makeText(getActivity().getApplicationContext(), "Register Success!", Toast.LENGTH_LONG).show();
                            }

                        }
                    });*/


                db.collection("Users").document(user.getUid()).set(usermap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity().getApplicationContext(), "Register Success!", Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "FireStore Failed!", Toast.LENGTH_LONG).show();
                    }
                });




                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Register Unsuccessful!", Toast.LENGTH_LONG).show();
                }

            }
        });


        FirebaseAuth.getInstance().signOut();
        NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.loginfragment);
    }

    public boolean checkEmptyField() {
        if (TextUtils.isEmpty(edt_email.getText().toString())) {
            edt_email.setError("Email Cannot be Empty!");
            edt_email.requestFocus();

            return true;

        } else if (TextUtils.isEmpty(edt_pass.getText().toString())) {
            edt_pass.setError("Password Cannot be Empty!");
            edt_pass.requestFocus();

            return true;
        }else if (TextUtils.isEmpty(edt_cPass.getText().toString())) {
            edt_cPass.setError("Confirm Password Cannot be Empty!");
            edt_cPass.requestFocus();
            return true;

        }else if (TextUtils.isEmpty(edt_name.getText().toString())) {
            edt_name.setError("Name Cannot be Empty!");
            edt_name.requestFocus();
            return true;
        }

        return false;
    }

}
