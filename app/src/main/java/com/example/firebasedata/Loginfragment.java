package com.example.firebasedata;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;



public class Loginfragment extends Fragment implements View.OnClickListener{


    private FirebaseAuth auth;
    private FirebaseUser curUser;
    EditText edt_email,edt_pass;
    TextView txt_register;
    Button btn_login;


    private static final String TAG = "Loginfragment";

    public Loginfragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_loginfragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        edt_email = view.findViewById(R.id.edt_loginEmail);
        edt_pass = view.findViewById(R.id.edt_loginPass);
        txt_register = view.findViewById(R.id.txt_loginRegister);
        btn_login = view.findViewById(R.id.btn_login);

        txt_register.setOnClickListener(this);
        btn_login.setOnClickListener(this);

    }


    @Override
    public void onStart() {
        super.onStart();

        Log.d(TAG, "onStart Called! ");

        curUser = auth.getCurrentUser();

        if (curUser != null)
        {
            updateUI(curUser);
            Toast.makeText(getActivity().getApplicationContext(),"User Already Login",Toast.LENGTH_LONG).show();
        }

    }

    public void loginUser(String email, String pass)
    {

        AuthCredential credential = EmailAuthProvider.getCredential(email,pass);

        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {

                    Toast.makeText(getActivity().getApplicationContext(),"Login Success!",Toast.LENGTH_LONG).show();
                    curUser = auth.getCurrentUser();
                    updateUI(curUser);

                }else {
                    Toast.makeText(getActivity().getApplicationContext(),"Login Failed!",Toast.LENGTH_LONG).show();
                }
            }
        });


     /*  auth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
           @Override
           public void onComplete(@NonNull Task<AuthResult> task) {
               if (task.isSuccessful())
               {

                   Toast.makeText(getActivity().getApplicationContext(),"Login Success!",Toast.LENGTH_LONG).show();
                   curUser = auth.getCurrentUser();
                        updateUI(curUser);

               }else {
                   Toast.makeText(getActivity().getApplicationContext(),"Login Failed!",Toast.LENGTH_LONG).show();
               }
           }
       });*/
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.btn_login)
        {
            if (TextUtils.isEmpty(edt_email.getText().toString()))
            {

                edt_email.setError("Email Cannot be blank!");
                edt_email.requestFocus();
                return;

            }else if (TextUtils.isEmpty(edt_pass.getText().toString()))
            {
                edt_pass.setError("Password Cannot be blank!");
                edt_pass.requestFocus();
                return;

            }else {
                String email = edt_email.getText().toString();
                String pass = edt_pass.getText().toString();
                loginUser(email,pass);
            }




        }else if (id == R.id.txt_loginRegister)
        {
            NavController navController = Navigation.findNavController(getActivity(),R.id.nav_host_fragment);
            navController.navigate(R.id.registerfragment);
        }



    }

    public void updateUI(FirebaseUser fUser)
    {
        NavController navController = Navigation.findNavController(getActivity(),R.id.nav_host_fragment);
        Bundle bundle = new Bundle();
        bundle.putParcelable("User",fUser);

        navController.navigate(R.id.dashboardfragment,bundle);
    }
}

